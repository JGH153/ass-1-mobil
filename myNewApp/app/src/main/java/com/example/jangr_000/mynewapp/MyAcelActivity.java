package com.example.jangr_000.mynewapp;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MyAcelActivity extends Activity implements SensorEventListener {

    private TextView textAreaX, textAreaY, textAreaZ;

    private SensorManager mSensorManager;
    private Sensor mSensor;

    private float mLastX, mLastY, mLastZ;

    private final float NOISE = (float)2.0;

    boolean mInitialized = false;

    private Sensor mAccelerometer;


    private float savedX, savedY, savedZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_acel);

        textAreaX      = (TextView) findViewById(R.id.textAreaX);
        textAreaY      = (TextView) findViewById(R.id.textAreaY);
        textAreaZ      = (TextView) findViewById(R.id.textAreaZ);

        mLastX = 0;
        mLastY = 0;
        mLastZ = 0;


        savedX = 0;
        savedY = 0;
        savedZ = 0;

        textAreaX.setText("test");

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);






        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);


        //textArea.setText(mSensor.toString());

    }


    @Override

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

        // can be safely ignored for this demo

    }



    @Override

    public void onSensorChanged(SensorEvent event) {



        float x = event.values[0];

        float y = event.values[1];

        float z = event.values[2];


        float deltaX = Math.abs(mLastX - x);

        float deltaY = Math.abs(mLastY - y);

        float deltaZ = Math.abs(mLastZ - z);

        if (deltaX < NOISE) {
            deltaX = (float) 0.0;

        }

        savedX = deltaX;


        if (deltaY < NOISE) {
            deltaY = (float) 0.0;

        }

        savedY = deltaY;

        if (deltaZ < NOISE) {
            deltaZ = (float) 0.0;

        }

        savedZ = deltaZ;

        mLastX = x;

        mLastY = y;

        mLastZ = z;

        //textArea.setText("X: " + Float.toString(deltaX) + " Y: " + Float.toString(deltaY) + " Z: " + Float.toString(deltaZ));

        textAreaX.setText(Float.toString(savedX));

        textAreaY.setText(Float.toString(savedY));

        textAreaZ.setText(Float.toString(savedZ));



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_acel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
