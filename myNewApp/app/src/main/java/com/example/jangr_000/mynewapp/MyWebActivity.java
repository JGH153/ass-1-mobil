package com.example.jangr_000.mynewapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;


public class MyWebActivity extends Activity implements SensorEventListener {

    private TextView showText;
    private Button generate;

    // file name for internal storage
    private String FILENAME = "Storage";

    private SensorManager mSensorManager;
    private Sensor mSensor;

    private float mLastX, mLastY, mLastZ;

    private final float NOISE = (float)2.0;

    boolean mInitialized = false;

    private Sensor mAccelerometer;

    public static boolean currentlyLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_web);

        generate          = (Button) findViewById(R.id.showText);
        showText      = (TextView) findViewById(R.id.textOutput);

        // set storage text
        showText.setText(getFile());

        // Read a webpage
        generate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // extract information about the webpage
                // check if the device have internett
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                // check if we have network connection
                if (networkInfo != null && networkInfo.isConnected()) {
                    // read from webpage when the device is connected to
                    // the network
                    readWebpage(showText);

                } else {
                    // set text to saved instance from the file, and add
                    // a message about the network!
                    showText.setText(
                            "No network connection available. Please turn on to " +
                                    "generate another quote from this genius");
                }
            }
        });


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }


    @Override

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

        // can be safely ignored for this demo

    }





    private int myCounter = 0;
    @Override
    public void onSensorChanged(SensorEvent event) {



        float x = event.values[0];

        float y = event.values[1];

        float z = event.values[2];


        float deltaX = Math.abs(mLastX - x);

        float deltaY = Math.abs(mLastY - y);

        float deltaZ = Math.abs(mLastZ - z);

        if (deltaX < NOISE) {
            deltaX = (float) 0.0;

        }


        if (deltaY < NOISE) {
            deltaY = (float) 0.0;

        }

        if (deltaZ < NOISE) {
            deltaZ = (float) 0.0;

        }

        mLastX = x;

        mLastY = y;

        mLastZ = z;

        //textArea.setText("X: " + Float.toString(deltaX) + " Y: " + Float.toString(deltaY) + " Z: " + Float.toString(deltaZ));

        float minumimRef = 10.0f;

        //showText.setText(Float.toString(deltaX));


        if(deltaX > minumimRef || deltaY > minumimRef || deltaZ > minumimRef ) {

            if(currentlyLoading == false) {

                currentlyLoading = true;

                myCounter ++;

                showText.setText("LOADING: "+ Integer.toString(myCounter) + " | " + Boolean.toString(currentlyLoading));

                MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.laugh);
                mediaPlayer.start(); // no need to call prepare(); create() does that for you



                // extract information about the webpage
                // check if the device have internett
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                // check if we have network connection
                if (networkInfo != null && networkInfo.isConnected()) {
                    // read from webpage when the device is connected to
                    // the network
                    readWebpage(showText);

                } else {
                    // set text to saved instance from the file, and add
                    // a message about the network!
                    showText.setText(
                            "No network connection available. Please turn on to " +
                                    "generate another quote from this genius");
                }

            }

        }




    }


    // Class to help operate in the background
    class MyAsyncTask extends AsyncTask<String, Void, String> {
        // Class to help operate in the background
        @Override
        protected String doInBackground(String... urls) {
            // string variables
            StringBuilder response = new StringBuilder(); // Build a string
            String str = ""; // string that read each line
            String[] quotes;  // array with the quotes generated

            // get the URL and create a HttpGet variable
            for (String url : urls) {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);

                // try to execute the URL and get the content
                try {
                    HttpResponse execute = client.execute(httpGet);
                    InputStream content = execute.getEntity().getContent();

                    // read from the webpage
                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(content, "UTF-8"));

                    // Read each line as long it isn't null
                    while ((str = buffer.readLine()) != null) {
                        // Build the string with string builder
                        response.append(str);
                    }

                    // catch exception
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Get the quotes from the html code from the webpage



            /*quotes = getQuotes(response.toString());

            // Import a random numbers to generate a random quote
            Random rand = new Random();

            // Import current time and date
            String time = DateFormat.getDateTimeInstance().format(new Date());

            // return the generated and modified string
            return (quotes[rand.nextInt(98)+2] + "\n" + time);
            */

            String responseModified = response.toString();
            responseModified = responseModified.substring(0, responseModified.indexOf('['));


            return (responseModified);

        }

        @Override // Set a quote to the text view!
        protected void onPostExecute(String result) {

            try {
                saveToFile(result);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // set text to genreated text
            showText.setText(result);
        }
    }

    // Function that read the web page
    public void readWebpage(View view) {

        // create a new Async task
        MyAsyncTask task = new MyAsyncTask();

        // execute from this page
        //task.execute(new String[] {"http://www.eviloverlord.com/lists/overlord.html"});
        task.execute(new String[] {"http://www.iheartquotes.com/api/v1/random"});

        currentlyLoading = false;

    }



    // function that split the generated string by "<LI>"
    // and put them in a string []
    public String[] getQuotes(String temp){
        String[] quotes = temp.split("<LI>");

        //Removing the annoying "<P>" embedded in the quote. Should probably be done differently, but it seems to work.
        for(int i = 2; i <= 100; i++){
            quotes[i] =  quotes[i].replace("<P>", "");

        }
        return quotes;
    }

    // Save to the internal Storage file
    public void saveToFile(String save) throws IOException{
        FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
        fos.write(save.getBytes());
        fos.close();
    }

    // Get the file from the internal storage!
    public String getFile(){

        // string to put our content
        String result = "";

        try{

            // variables to read from tile file
            FileInputStream inputStream = openFileInput(FILENAME);
            BufferedReader r = new BufferedReader
                    (new InputStreamReader(inputStream));

            // variables to put the content we read from the file
            StringBuilder total = new StringBuilder();
            String line;

            // read each line and it to the string
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            // close the stream
            r.close();

            // close the input stream
            inputStream.close();

            // set and convert the string builder to the string
            result = total.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // return the content from the file
        return result;
    }
}
